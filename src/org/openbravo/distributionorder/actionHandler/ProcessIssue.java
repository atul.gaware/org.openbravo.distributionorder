/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017-2020 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.actionHandler;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.client.application.process.ResponseActionsBuilder.MessageType;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.DistributionOrderLine;
import org.openbravo.distributionorder.erpCommon.utility.DistributioOrderUtils;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.materialmgmt.StockUtils;
import org.openbravo.model.materialmgmt.onhandquantity.StockProposed;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.service.db.DbUtility;

public class ProcessIssue extends BaseProcessActionHandler {
  private static final Logger log = Logger.getLogger(ProcessIssue.class);

  @Override
  protected JSONObject doExecute(final Map<String, Object> parameters, final String content) {
    try {
      final JSONObject jsonData = new JSONObject(content);
      final String recordID = jsonData.getString("M_Movement_ID");

      final InternalMovement movement = OBDal.getInstance().get(InternalMovement.class, recordID);
      return processIssueDO(movement);

    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      return getResponseBuilder()
          .showMsgInProcessView(MessageType.ERROR, OBMessageUtils.messageBD("Error"),
              OBMessageUtils.translateError(DbUtility.getUnderlyingSQLException(e).getMessage())
                  .getMessage())
          .build();
    }
  }

  public static JSONObject processIssueDO(final InternalMovement movement) {
    OBContext.setCrossOrgReferenceAdminMode();
    try {
      if (movement.getObdoIntransitLocator() == null) {
        return getResponseBuilder()
            .showMsgInProcessView(MessageType.ERROR, OBMessageUtils.messageBD("Error"),
                OBMessageUtils.messageBD("OBDO_TRANSIT_BIN"))
            .build();
      }

      StringBuilder errorsWhileAddingLines = createMovementlines(movement);
      if (errorsWhileAddingLines.length() != 0) {
        return getResponseBuilder()
            .showMsgInProcessView(MessageType.ERROR, OBMessageUtils.messageBD("Error"),
                errorsWhileAddingLines.toString())
            .build();
      }

      JSONObject errorsWhileProcessingDO = DistributioOrderUtils.processMovement(movement);
      if (errorsWhileProcessingDO != null) {
        return errorsWhileProcessingDO;
      }

      DistributioOrderUtils.updateDistributionOrder(movement);
      return getResponseBuilder()
          .showMsgInProcessView(MessageType.SUCCESS, OBMessageUtils.messageBD("Success"),
              OBMessageUtils.messageBD("Success"))
          .build();
    } finally {
      OBContext.restorePreviousCrossOrgReferenceMode();
    }
  }

  /**
   * Creates and adds the lines from the Distribution Order into the Goods Movement that is being
   * created
   * 
   * If there is already a line in the Goods Movement with the same Product as the Distribution
   * Order line, it keeps the Goods Movement line over the one in the Distribution Order
   * 
   * If there is a line in the Goods Movement for a Product not present in the Distribution Order it
   * fails with a message to the user
   * 
   * When creating the lines for the Goods Movement, it retrieves available Stock from all possible
   * locations except from the Bin defined as In Transit in the Document
   * 
   * @param movement
   *          a Goods Movement object that is going to contain the created Goods Movement Lines
   */

  private static StringBuilder createMovementlines(final InternalMovement movement) {
    ScrollableResults linesDO = null;
    try {
      final DistributionOrder distributionOrder = movement.getObdoDistorder();

      linesDO = DistributioOrderUtils.getDistOrderLinesScrollableResult(distributionOrder);

      long lineno = (movement.getMaterialMgmtInternalMovementLineList().size() * 10) + 10;
      int i = 0;
      boolean doIssueHasPositiveConfirmedLines = false;
      final List<InternalMovementLine> movlines = movement
          .getMaterialMgmtInternalMovementLineList();
      final StringBuilder resultMessage = new StringBuilder();

      while (linesDO.next()) {
        doIssueHasPositiveConfirmedLines = true;
        int linesadd = 0;
        final DistributionOrderLine distOrdline = (DistributionOrderLine) linesDO.get(0);
        final boolean needsToCreateNewLine = hasToCreateNewLineForDOLine(distOrdline, movlines,
            resultMessage);

        ScrollableResults stockProposed = null;

        if (!needsToCreateNewLine) {
          if (resultMessage.length() > 0) {
            return resultMessage;
          }
        } else {
          try {
            stockProposed = getStockProposedForDOLine(movement, distOrdline);

            BigDecimal pendingQty = distOrdline.getQtyConfirmed()
                .subtract(distOrdline.getQtyIssued());
            while (stockProposed.next()) {

              final StockProposed stock = (StockProposed) stockProposed.get(0);
              final BigDecimal qtyOnHand = stock.getStorageDetail().getQuantityOnHand();

              if (distOrdline.getQtyConfirmed().intValue() > 0
                  && pendingQty.compareTo(BigDecimal.ZERO) > 0) {
                final BigDecimal movementQty = qtyOnHand.min(pendingQty);
                createAndSaveMovementLine(movement, distOrdline, stock, movementQty, lineno);

                pendingQty = pendingQty.subtract(movementQty);
                linesadd++;
                lineno += 10;
              }
            }

            if (linesadd == 0) {
              if (resultMessage.length() == 0) {
                resultMessage.append(OBMessageUtils.messageBD("OBDO_STOCK"));
              }
              resultMessage.append(" " + distOrdline.getProduct().getName());
            }
            i++;

            if ((i % 100) == 0) {
              OBDal.getInstance().flush();
              OBDal.getInstance().getSession().clear();
            }

          } catch (Exception e) {
            log.error("Error in ProcessIssue.createMovementlines method", e);
            throw e;
          } finally {
            stockProposed.close();
          }
        }
      }

      if (!doIssueHasPositiveConfirmedLines) {
        return new StringBuilder(OBMessageUtils.messageBD("OBDO_NoConfirmedLines"));
      }

      if (movement.getDescription() == (null) || movement.getDescription().trim().equals("")) {
        movement.setDescription("Automatically generated by Distribution Order "
            + movement.getObdoDistorder().getDocumentNo());
      }

      return resultMessage;

    } finally {
      linesDO.close();
    }

  }

  /**
   * Return true if it has to create a new Movement Line for the related Order Line Also, add error
   * messages to the result if there are reasons for not creating the line
   * 
   * @param distOrdLine
   *          A Distribution Order Line object
   * @param movlines
   *          A list of Goods Movement Lines
   * @param resultMessage
   *          A StringBuilder that holds the different result messages concatenated
   * @return
   */
  private static boolean hasToCreateNewLineForDOLine(final DistributionOrderLine distOrdLine,
      final List<InternalMovementLine> movlines, final StringBuilder resultMessage) {
    for (InternalMovementLine movLine : movlines) {

      if (!hasRelatedDO(movLine)) {
        resultMessage.append(OBMessageUtils.messageBD("OBDO_REFDOI") + " " + movLine.getLineNo());
      }

      if (!productIsSameAsInRelatedDO(movLine)) {
        resultMessage
            .append(String.format(OBMessageUtils.messageBD("OBDO_PRODUCTS"), movLine.getLineNo()));
      }

      return (hasRelatedDO(movLine) && productIsSameAsInRelatedDO(movLine)
          && !movementLineHasSameProduct(movLine, distOrdLine));
    }
    return true; // No MovementLines
  }

  /**
   * Returns true if the given Movement Line has a Distribution Order related to it
   * 
   * @param movLine
   *          A Movement Line object
   */
  private static boolean hasRelatedDO(final InternalMovementLine movLine) {
    return movLine.getObdoDistorderline() != null;
  }

  /**
   * Returns true if the Goods Movement Line has the same Product as the Distribution Order Line
   * 
   * @param movLine
   *          A Goods Movement Line object
   * @param distOrdLine
   *          A Distribution Order Line object
   */
  private static boolean movementLineHasSameProduct(final InternalMovementLine movLine,
      final DistributionOrderLine distOrdLine) {
    return movLine.getProduct().equals(distOrdLine.getProduct());
  }

  /**
   * Returns true if the Product of the Movement Line is the same one as the related DO Line
   * 
   * @param movLine
   * @return
   */
  private static boolean productIsSameAsInRelatedDO(final InternalMovementLine movLine) {
    return (movLine.getObdoDistorderline() != null && movLine.getProduct()
        .getId()
        .equals(movLine.getObdoDistorderline().getProduct().getId()));
  }

  /**
   * Returns the Stock Proposed for the given distribution order line based on the created Goods
   * Movement
   * 
   * @param movement
   *          A Goods Movement Object
   * @param distOrdline
   *          A Distribution Order Line Object
   */
  private static ScrollableResults getStockProposedForDOLine(final InternalMovement movement,
      final DistributionOrderLine distOrdline) {
    final String pInstanceID = callProcessGetStock(distOrdline.getId(),
        distOrdline.getClient().getId(), distOrdline.getOrganization().getId(),
        distOrdline.getProduct().getId(), distOrdline.getUOM().getId(),
        distOrdline.getDistributionOrder().getWarehouseIssue().getId(), null,
        distOrdline.getQtyIssued(), null, null);

    //@formatter:off
    final String hql =
                  " as sp" +
                  " join sp.storageDetail as sd" +
                  " join sd.storageBin as sb" +
                  " where sp.processInstance= :pinstanceId" +
                  " and sb.id <> :storageBinId" +
                  " order by sp.priority asc";
    //@formatter:off
    
    return OBDal.getInstance()
        .createQuery(StockProposed.class, hql)
        .setNamedParameter("pinstanceId", pInstanceID)
        .setNamedParameter("storageBinId", movement.getObdoIntransitLocator().getId())
        .scroll(ScrollMode.SCROLL_INSENSITIVE);
  }

  /**
   * Make a search to obtain the stock and the information of the bin of the product, in a certain
   * store corresponding to an organization
   * 
   * @param recordID
   *          Id of the line where you want to obtain the stock
   * @param clientId
   *          Client using openbravo
   * @param orgId
   *          Organization ID
   * @param productId
   *          Product ID
   * @param uomId
   *          Uom ID related to the product
   * @param warehouseId
   *          Id of the warehouse from where the product will be obtained
   * @param attributesetinstanceId
   *          attributeinstance ID, maybe null
   * @param quantity
   *          Required Quantity of Product
   * @param warehouseRuleId
   *          rulearehouse ID, maybe null
   * @param reservationId
   *          reservation ID, maybe null
   * @return The pinstanceid, from which you will get the result in StockProposed
   */

  private static String callProcessGetStock(final String recordID, final String clientId, final String orgId,
      final String productId, final String uomId, final String warehouseId, final String attributesetinstanceId,
      final BigDecimal quantity, final String warehouseRuleId, final String reservationId) {
    final String processId = SequenceIdData.getUUID();
    OBContext.setAdminMode(false);
    try {
      if (log.isDebugEnabled()) {
        log.debug("Parameters : '" + processId + "', '" + recordID + "', " + quantity + ", '"
            + productId + "', '" + warehouseId + "',  null, null, '" + orgId + "', '"
            + attributesetinstanceId + "', '" + OBContext.getOBContext().getUser().getId() + "', '"
            + clientId + "', '" + warehouseRuleId + "', '" + uomId
            + "', null, null, null, null, null, '" + reservationId + "', 'N'");
      }
      final long initGetStockProcedureCall = System.currentTimeMillis();
      StockUtils.getStock(processId, recordID, quantity, productId, null, warehouseId, null, orgId,
          attributesetinstanceId, OBContext.getOBContext().getUser().getId(), clientId,
          warehouseRuleId, uomId, null, null, null, null, null, reservationId, "N");
      final long elapsedGetStockProcedureCall = (System.currentTimeMillis() - initGetStockProcedureCall);
      if (log.isDebugEnabled()) {
        log.debug("Partial time to execute callGetStock Procedure Call() : "
            + elapsedGetStockProcedureCall);
      }
      return processId;
    } catch (Exception ex) {
      throw new OBException(
          "Error when getting stock for product " + productId + " order line " + recordID, ex);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Creates and Saves a Goods Movement Line using the given parameters
   * 
   * @param movement
   *          The Goods Movement to which the line is going to be related
   * @param distOrdline
   *          The Distribution Order Line for which the Movement Line is going to be created
   * @param stock
   *          The Stock Proposed to be used in the Movement Line
   * @param movementQty
   *          The Movement Quantity of the Goods Movement Line
   * @param lineno
   *          The Line No. of the created Movement Line
   * @return
   */
  private static InternalMovementLine createAndSaveMovementLine(final InternalMovement movement,
      final DistributionOrderLine distOrdline, final StockProposed stock, final BigDecimal movementQty, final long lineno) {
    final InternalMovementLine movementline = OBProvider.getInstance().get(InternalMovementLine.class);
    movementline.setOrganization(movement.getOrganization());
    movementline.setClient(movement.getClient());
    movementline.setMovement(movement);
    movementline.setProduct(distOrdline.getProduct());
    movementline.setLineNo(lineno);
    movementline.setMovementQuantity(movementQty);
    movementline.setAttributeSetValue(stock.getStorageDetail().getAttributeSetValue());
    movementline.setUOM(distOrdline.getUOM());
    movementline.setObdoDistorderline(distOrdline);
    movementline.setStorageBin(stock.getStorageDetail().getStorageBin());
    movementline.setNewStorageBin(movement.getObdoIntransitLocator());

    movementline.setOperativeQuantity(distOrdline.getOperativeQuantity());
    movementline.setAlternativeUOM(distOrdline.getAlternativeUOM());

    OBDal.getInstance().save(movementline);
    return movementline;
  }

}
