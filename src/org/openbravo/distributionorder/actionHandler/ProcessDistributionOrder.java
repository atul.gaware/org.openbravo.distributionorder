/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017-2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */
package org.openbravo.distributionorder.actionHandler;

import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.erpCommon.utility.ProcessDistributionOrderUtil;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBMessageUtils;

public class ProcessDistributionOrder extends BaseProcessActionHandler {

  private static final Logger log = Logger.getLogger(ProcessDistributionOrder.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    OBError msg = new OBError();
    JSONObject result = new JSONObject();
    try {

      final JSONObject jsonData = new JSONObject(content);

      String strDistOrderId = jsonData.getString("inpobdoDistorderId");
      DistributionOrder distOrder = OBDal.getInstance()
          .get(DistributionOrder.class, strDistOrderId);

      msg = ProcessDistributionOrderUtil.processDistributionOrder(distOrder);
      result = createReturnMessage(msg);

    } catch (JSONException e) {

      log.error(e.getMessage(), e);
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      try {

        JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", "error");
        errorMessage.put("title", OBMessageUtils.messageBD("Error"));
        errorMessage.put("text", e.getMessage());
        result.put("message", errorMessage);
      } catch (JSONException e2) {

        log.error(e2.getMessage(), e2);
      }
    }

    return result;
  }

  private JSONObject createReturnMessage(OBError msg) {
    JSONObject jsonRequest = new JSONObject();
    try {
      JSONObject successMessage = new JSONObject();
      successMessage.put("severity", msg.getType().toLowerCase());
      successMessage.put("title", msg.getTitle());
      successMessage.put("text", msg.getMessage());
      jsonRequest.put("message", successMessage);

    } catch (JSONException e) {
      e.printStackTrace();
    }
    return jsonRequest;
  }

}
