/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.info;

import java.util.Map;

import org.apache.log4j.Logger;
import org.openbravo.client.application.FilterExpression;
import org.openbravo.dal.core.OBContext;
import org.openbravo.distributionorder.erpCommon.utility.DistributioOrderUtils;

public class WarehouseReceiptSelectorFilterExpression implements FilterExpression {

  private static final Logger log = Logger
      .getLogger(WarehouseReceiptSelectorFilterExpression.class);

  @Override
  public String getExpression(Map<String, String> requestMap) {

    try {
      OBContext.setAdminMode(true);

      String organizationId = requestMap.get("inpadOrgId");
      String warehouseIssueID = requestMap.get("inpmWarehouseIssueId");
      boolean isSalesTransaction = Boolean.valueOf(requestMap.get("inpissotrx"));

      return DistributioOrderUtils.getFilterExpressionForReceipt(organizationId, warehouseIssueID,
          isSalesTransaction);

    } catch (Exception e) {
      log.debug("Error in WarehouseReceiptSelectorFilterExpression.getExpression method", e);
      return null;
    } finally {
      OBContext.restorePreviousMode();
    }
  }
}
