/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017-2020 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.erpCommon.utility;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidParameterException;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.security.OrganizationStructureProvider;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.DistributionOrderLine;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.ad.ui.Process;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.service.db.CallProcess;
import org.openbravo.service.db.DbUtility;

public class DistributioOrderUtils {

  private static final Logger log = Logger.getLogger(DistributioOrderUtils.class);
  private static final String PROCESS_ID = "122";
  public static final String DO_ISSUE_LINE_CREATED_FROM_PICKING = "DO_ISSUE_LINE_CREATED_FROM_PICKING";

  /**
   * It is called a process to complete the goods movements
   * 
   * @param movement
   *          A goods movement object that will contain the goods movement lines created
   */

  public static JSONObject processMovement(final InternalMovement movement) {
    JSONObject jsonRequest = null;
    try {
      final Process process = OBDal.getInstance().get(Process.class, PROCESS_ID);
      final ProcessInstance pinstance = CallProcess.getInstance()
          .call(process, movement.getId(), null);
      if (pinstance.getResult() == 0L) {
        throw new OBException(OBMessageUtils.messageBD("ErrorProcessingGoodMovement") + ". "
            + OBMessageUtils.getProcessInstanceMessage(pinstance).getMessage());
      }
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      OBDal.getInstance().rollbackAndClose();
      try {
        jsonRequest = new JSONObject();
        final Throwable sqlException = DbUtility.getUnderlyingSQLException(e);
        final String message = OBMessageUtils.translateError(sqlException.getMessage())
            .getMessage();
        final JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", "error");
        errorMessage.put("title", OBMessageUtils.messageBD("Error"));
        errorMessage.put("text", message);
        jsonRequest.put("message", errorMessage);

      } catch (Exception e2) {
        log.error(e2.getMessage(), e2);
      }
    }

    OBDal.getInstance().refresh(movement);
    return jsonRequest;
  }

  /**
   * Updates the quantity received in the Distribution order issue / receipt, with the quantities
   * received in this Goods movement.
   * 
   * Performs the calculation of the received percentage and updates it in the distribution order
   * receipt
   * 
   * @param movement
   *          A goods movement object that will contain the goods movement lines created
   */

  public static void updateDistributionOrder(final InternalMovement movement) {
    ScrollableResults movlLinesSR = null;
    try {
      OBContext.setAdminMode(false);
      movlLinesSR = getMovementLinesScrollableResults(movement);
      int i = 0;

      while (movlLinesSR.next()) {
        final InternalMovementLine movLine = (InternalMovementLine) movlLinesSR.get(0);

        final DistributionOrderLine distOrderLineIssue = movLine.getObdoDistorderline();
        updateDOLineReceipAndIssueQty(distOrderLineIssue, movLine);
        final DistributionOrderLine distOrderLineReceipt = distOrderLineIssue
            .getReferencedDistorderline();
        updateDOLineReceipAndIssueQty(distOrderLineReceipt, movLine);
        i++;

        if ((i % 100) == 0) {
          OBDal.getInstance().flush();
          OBDal.getInstance().getSession().clear();
          // Refresh movement after session clear to use it again
          OBDal.getInstance().refresh(movement);
        }
      }
      updateDOIssueStatusAfterMovement(movement);
      OBDal.getInstance().flush();
    } finally {
      movlLinesSR.close();
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Updates the quantity received in the Distribution order line issue / receipt, with the
   * quantities received in this Goods movement line.
   * 
   * Performs the calculation of the received percentage and updates it in the distribution order
   * receipt
   * 
   * @param movLine
   *          A goods movement line object
   */
  public static void updateDistributionOrder(final InternalMovementLine movLine) {
    final DistributionOrderLine distOrderLineIssue = movLine.getObdoDistorderline();
    updateDOLineReceipAndIssueQty(distOrderLineIssue, movLine);
    final DistributionOrderLine distOrderLineReceipt = distOrderLineIssue
        .getReferencedDistorderline();
    updateDOLineReceipAndIssueQty(distOrderLineReceipt, movLine);
    updateDOIssueStatusAfterMovement(movLine.getMovement());
  }

  /**
   * Updates the Qty Issued/Received of the given Issue DO Line based on the parameters
   * 
   * @param distOderLine
   *          The Issue DO Line object to be updated
   * @param movLine
   *          A Goods Movement Line related to the Issue DO Line
   */
  private static void updateDOLineReceipAndIssueQty(final DistributionOrderLine distOderLine,
      final InternalMovementLine movLine) {
    final InternalMovement movement = movLine.getMovement();
    if (movement.isObdoIssotrx()) {
      distOderLine.setQtyIssued(distOderLine.getQtyIssued().add(movLine.getMovementQuantity()));
    } else {
      distOderLine.setQtyReceived(distOderLine.getQtyReceived().add(movLine.getMovementQuantity()));
    }
    OBDal.getInstance().save(distOderLine);
  }

  /**
   * Updates the Issue Status of the DO related to the Goods Movement parameter
   * 
   * @param movement
   *          A Goods Movement with a DO related
   * @param issueStatus
   *          The Issue Status that is going to be set to the DO
   */
  private static void updateDOIssueStatusAfterMovement(final InternalMovement movement) {
    // Refresh movement to retrieve latest changes done to the related lines
    if (movement.isObdoIssotrx()) {
      final DistributionOrder distOrder = movement.getObdoDistorder();
      long issueStatus = getIssueOrReceiptNewIssueStatusFor(distOrder);
      distOrder.setIssueStatus(issueStatus);
      OBDal.getInstance().save(distOrder);
    } else {
      final DistributionOrder distOrder = movement.getMaterialMgmtInternalMovementLineList()
          .get(0)
          .getObdoDistorderline()
          .getDistributionOrder();
      final long issueStatus = getIssueOrReceiptNewIssueStatusFor(distOrder);
      distOrder.setReceiptStatus(issueStatus);
      OBDal.getInstance().save(distOrder);
    }
  }

  /**
   * Returns the new Issue status for the given Distribution Order
   * 
   * @param distOrder
   *          A Distribution Order Object
   * @return a long containing the new Issue Status
   */
  public static long getIssueOrReceiptNewIssueStatusFor(final DistributionOrder distOrder) {
    BigDecimal confirmed = BigDecimal.ZERO;
    BigDecimal qtyMoved = BigDecimal.ZERO;
    ScrollableResults linesDO = null;
    try {
      linesDO = DistributioOrderUtils.getDistOrderLinesScrollableResult(distOrder);
      while (linesDO.next()) {
        final DistributionOrderLine distOrdline = (DistributionOrderLine) linesDO.get(0);
        confirmed = confirmed.add(distOrdline.getQtyConfirmed());
        if (distOrder.isSalesTransaction()) {
          qtyMoved = qtyMoved.add(distOrdline.getQtyIssued());
        } else {
          qtyMoved = qtyMoved.add(distOrdline.getQtyReceived());
        }
      }
      long issueOrReceiptStatus = 0L;
      if (!confirmed.equals(BigDecimal.ZERO)) {
        issueOrReceiptStatus = qtyMoved.divide(confirmed, 2, RoundingMode.HALF_UP)
            .multiply(new BigDecimal(100))
            .longValue();
      }
      return issueOrReceiptStatus;
    } finally {
      linesDO.close();
    }
  }

  /**
   * Returns the Movement Lines of the given Goods Movement
   * 
   * @param movement
   *          A Goods Movement Object
   * @return
   */
  private static ScrollableResults getMovementLinesScrollableResults(
      final InternalMovement movement) {
    return OBDal.getInstance()
        .createCriteria(InternalMovementLine.class)
        .add(Restrictions.eq(InternalMovementLine.PROPERTY_MOVEMENT, movement))
        .scroll(ScrollMode.FORWARD_ONLY);
  }

  /**
   * Get Distribution Order Lines from given Distribution Order parameter
   * 
   * @param distributionOrder
   *          A Distribution Order object
   * @return A Scrollable Result containing the Distribution Order Lines
   */
  public static ScrollableResults getDistOrderLinesScrollableResult(
      final DistributionOrder distributionOrder) {

    return OBDal.getInstance()
        .createCriteria(DistributionOrderLine.class)
        .add(Restrictions.eq(DistributionOrderLine.PROPERTY_DISTRIBUTIONORDER, distributionOrder))
        .add(Restrictions.gt(DistributionOrderLine.PROPERTY_QTYCONFIRMED, BigDecimal.ZERO))
        .scroll(ScrollMode.FORWARD_ONLY);
  }

  /**
   * Get the filter expression for the field warehouse issue
   * 
   * @param organizationID
   *          Organization of which will be obtained the tree of organizations within the same legal
   *          entity
   * @param warehouseID
   *          Id of the selected store in the other field
   * @param isSalesTransaction
   *          Indicates if the distribution order is Issue or Receipt
   */

  public static String getFilterExpressionForIssue(final String organizationID,
      final String warehouseID, final boolean isSalesTransaction) {
    return getFilterExpression(true, organizationID, warehouseID, isSalesTransaction);
  }

  /**
   * Get the filter expression for the field warehouse receipt
   * 
   * @param organizationID
   *          Organization of which will be obtained the tree of organizations within the same legal
   *          entity
   * @param warehouseID
   *          Id of the selected store in the other field
   * @param isSalesTransaction
   *          Indicates if the distribution order is Issue or Receipt
   */

  public static String getFilterExpressionForReceipt(final String organizationID,
      final String warehouseID, final boolean isSalesTransaction) {
    return getFilterExpression(false, organizationID, warehouseID, isSalesTransaction);
  }

  /**
   * Add the filters to get the stores associated with the organization depending on whether it is a
   * DO issue or Receipt
   * 
   * Reviews the natural tree of the organization from the legal entity
   * 
   * @param isIssueSelector
   *          Indicates if the selector is of a DO issue
   * @param organizationID
   *          Organization of which will be obtained the tree of organizations within the same legal
   *          entity
   * @param warehouseID
   *          Id of the selected store in the other field
   * @param isSalesTransaction
   *          Indicates if the distribution order is Issue or Receipt
   * @return Returns a string with the filters obtained
   */

  private static String getFilterExpression(final boolean isIssueSelector,
      final String organizationID, final String warehouseID, final boolean isSalesTransaction) {

    String whereClause = "";
    final Organization org = OBDal.getInstance().get(Organization.class, organizationID);
    final OrganizationStructureProvider organization = new OrganizationStructureProvider();
    final Organization legalEntity = organization.getLegalEntity(org);
    final Set<String> orgTree = organization.getChildTree(legalEntity.getId(), true);

    String strOrgIDs = "";
    for (Object result : orgTree) {
      strOrgIDs += "'" + result.toString() + "',";
    }

    strOrgIDs = strOrgIDs.substring(0, strOrgIDs.length() - 1);
    if (isIssueSelector) {
      if (isSalesTransaction) {
        if (org.getOrganizationType().isLegalEntity()) {
          whereClause += "e.organization.id in (" + strOrgIDs + ")";
        } else {
          whereClause += "e.organization.id ='" + organizationID + "'";
        }
      } else {
        whereClause += "e.organization.id in (" + strOrgIDs + ") AND e.id <>'" + warehouseID + "'";
      }
    } else {
      if (isSalesTransaction) {
        whereClause += "e.organization.id in (" + strOrgIDs + ") AND e.id <>'" + warehouseID + "'";
      } else {
        if (org.getOrganizationType().isLegalEntity()) {
          whereClause += "e.organization.id in (" + strOrgIDs + ")";
        } else {
          whereClause += "e.organization.id ='" + organizationID + "'";
        }

      }
    }

    return whereClause;
  }

  /**
   * Returns the business partner id associated to the referenced distribution order line of the
   * given distOrderLine (distOrderLine.referencedDistOrderLine).
   * 
   * A business partner can be associated to an organization in the Organization | Information tab.
   * 
   * The method browses to the distOrderLine.referencedDistOrderLine to get its organization and
   * returns its associated business partner. If not defined, returns null.
   */
  public static String getBusinessPartnerIdFromReferencedDistributionOrderLine(
      final DistributionOrderLine distOrderLine) {
    try {
      OBContext.setAdminMode(true);
      if (distOrderLine == null) {
        throw new InvalidParameterException("distOrderIssueLine parameter can't be null");
      }

      final DistributionOrderLine distOrderReceiveLine = distOrderLine.getReferencedDistorderline();
      if (distOrderReceiveLine == null) {
        throw new InvalidParameterException("Referenced Distribution Order Receipt can't be null");
      }

      // @formatter:off
      final String hql =  
              "select bp.id " + 
              "  from OBDO_DistributionOrderLine dol " + 
              "    join dol.organization o " + 
              "    join o.organizationInformationList oi " + 
              "    join oi.businessPartner bp " + 
              " where dol.id = :distributionOrderLineId ";
      // @formatter:on

      return (String) OBDal.getInstance()
          .getSession()
          .createQuery(hql)
          .setParameter("distributionOrderLineId", distOrderReceiveLine.getId())
          .uniqueResult();
    } catch (Exception noBusinessPartnerFound) {
      return null;
    } finally {
      OBContext.restorePreviousMode();
    }
  }
}
