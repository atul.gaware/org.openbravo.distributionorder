/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.test.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.DistributionOrderLine;
import org.openbravo.distributionorder.DistributionOrderV;
import org.openbravo.distributionorder.actionHandler.ProcessIssue;
import org.openbravo.distributionorder.actionHandler.ProcessReceipt;
import org.openbravo.distributionorder.erpCommon.utility.ProcessDistributionOrderUtil;
import org.openbravo.erpCommon.businessUtility.Preferences;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.ad.domain.Preference;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.ad.ui.Process;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.model.pricing.pricelist.ProductPrice;
import org.openbravo.service.db.CallProcess;

public class DistributionOrderTestUtils {

  // Client F&B International
  public static final String FB_CLIENT_ID = "23C59575B9CF467C9620760EB255B389";
  // Organization US
  public static final String US_ORG_ID = "2E60544D37534C0B89E765FE29BC0B43";
  // Organization US East Coast
  public static final String US_EAST_ORG_ID = "7BABA5FF80494CAFA54DEBD22EC46F01";
  // Organization US West Coast
  public static final String US_WEST_ORG_ID = "BAE22373FEBE4CCCA24517E23F0C8A48";
  // User Openbravo
  public static final String USER_ID = "100";
  // Role F&B Administrator
  public static final String ROLE_ID = "42D0EEB1C66F497A90DD526DC597E6F0";
  // Language encoding English US
  public static final String LANGUAGE_CODE = "en_US";

  // US West Coast Warehouse
  public static final String US_WC_WAREHOUSE_ID = "4D45FE4C515041709047F51D139A21AC";
  // US East Coast Warehouse
  public static final String US_EC_WAREHOUSE_ID = "9CF98A18BC754B99998E421F91C5FE12";
  // Unit UOM
  public static final String UNIT_UOM_ID = "100";
  // MM Receipt US Doctype
  public static final String MM_RECEIPT_US_DOCTYPE_ID = "2753CB9943A447AABDD2E8E6DA361F05";
  // EC-2-0-0 Locator
  public static final String EC_200_LOCATOR_ID = "28531C9B6A4D422E87A5888E2F9A51D4";
  // EC-0-0-0 Locator
  public static final String EC_000_LOCATOR_ID = "2E50CE5F5B2D4608AB7959C4CBB66178";
  // WC-0-0-0 Locator
  public static final String WC_000_LOCATOR_ID = "F83534CACA5E4387A5F87FCD3B28A91C";

  // Process Goods Receipt/Shipment
  public static final String PROCESS__M_MINOUT_POST = "109";

  // DocType Names
  private static final String DO_ISSUE_DOCTYPE_NAME = "Distribution Order Issue";
  private static final String DO_RECEIPT_DOCTYPE_NAME = "Distribution Order Receipt";

  // Document Status
  public static final String COMPLETED_DOCSTATUS = "CO";
  public static final String REQUESTED_DOCSTATUS = "RE";
  public static final String CONFIRMED_DOCSTATUS = "CF";
  public static final String DRAFT_DOCSTATUS = "DR";
  public static final String CLOSED_DOCSTATUS = "CL";

  /**
   * Returns a new Product which is a clone of the Product with the given Product ID, but has the
   * name given as a parameter. It also clones the Price Lists of the Original Product
   * 
   * @param oldProductID
   *          ID of the Product to be cloned
   * @param newProductName
   *          Name that is going to be set to the newly created Product
   * @return a Produt object that is a clone of the original one
   */
  public static Product cloneProduct(String oldProductID, String newProductName) {
    Product oldProduct = OBDal.getInstance().get(Product.class, oldProductID);
    Product newProduct = (Product) DalUtil.copy(oldProduct, false);
    int numberOfProductsWithSameName = getNumberOfProductsWithSameName(newProductName);
    newProduct.setSearchKey(newProductName + "-" + numberOfProductsWithSameName);
    newProduct.setName(newProductName + "-" + numberOfProductsWithSameName);
    OBDal.getInstance().save(newProduct);
    for (ProductPrice oldPrice : oldProduct.getPricingProductPriceList()) {
      ProductPrice newPrice = (ProductPrice) DalUtil.copy(oldPrice, false);
      newPrice.setProduct(newProduct);
      OBDal.getInstance().save(newPrice);
      newProduct.getPricingProductPriceList().add(newPrice);
    }
    OBDal.getInstance().flush();
    return newProduct;
  }

  /**
   * Returns the number of products with same Product name
   */
  public static int getNumberOfProductsWithSameName(String name) {
    final OBCriteria<Product> criteria = OBDal.getInstance().createCriteria(Product.class);
    criteria.add(Restrictions.like(Product.PROPERTY_NAME, name + "-%"));
    return criteria.list().size();
  }

  /**
   * Creates and saves a Distribution Order Header based on the data given as a parameter
   */
  public static DistributionOrder createAndSaveDistributionOrderHeader(
      DistributionOrderHeaderData distributionOrderHeaderData) {
    DistributionOrder distributionOrder = OBProvider.getInstance().get(DistributionOrder.class);
    distributionOrder.setClient(distributionOrderHeaderData.getOrganization().getClient());
    distributionOrder.setOrganization(distributionOrderHeaderData.getOrganization());
    distributionOrder.setOrderDate(distributionOrderHeaderData.getOrderDate());
    distributionOrder
        .setScheduledDeliveryDate(distributionOrderHeaderData.getScheduledDeliveryDate());
    distributionOrder.setWarehouseReceipt(distributionOrderHeaderData.getWarehouseReceipt());
    distributionOrder.setWarehouseIssue(distributionOrderHeaderData.getWarehouseIssue());
    distributionOrder.setSalesTransaction(distributionOrderHeaderData.isSalesTransaction());
    distributionOrder.setDocumentType(distributionOrderHeaderData.getDocType());
    distributionOrder.setDocumentNo(distributionOrderHeaderData.getDocNo());
    distributionOrder.setDescription(distributionOrderHeaderData.getDescription());
    OBDal.getInstance().save(distributionOrder);
    return distributionOrder;
  }

  /**
   * Creates and saves a Distribution Order Line based on the data given as a parameter. It links
   * the line to the given Distribution Order Parameter
   */
  public static DistributionOrderLine createAndInsertDistributionOrderLine(
      DistributionOrderLineData distributionOrderLineData, DistributionOrder distributionOrder) {
    DistributionOrderLine distributionOrderLine = OBProvider.getInstance()
        .get(DistributionOrderLine.class);
    distributionOrderLine.setClient(distributionOrderLineData.getOrganization().getClient());
    distributionOrderLine.setOrganization(distributionOrderLineData.getOrganization());
    distributionOrderLine.setLineNo(distributionOrderLineData.getLineNo());
    distributionOrderLine.setProduct(distributionOrderLineData.getProduct());
    distributionOrderLine.setUOM(distributionOrderLineData.getUom());
    distributionOrderLine.setOrderedQuantity(distributionOrderLineData.getQtyOrdered());
    distributionOrderLine.setAlternativeUOM(distributionOrderLineData.getAum());
    distributionOrderLine.setOperativeQuantity(distributionOrderLineData.getOperativeQty());
    distributionOrderLine.setQtyConfirmed(distributionOrderLineData.getQtyConfirmed());
    distributionOrderLine.setAttributeSetValue(distributionOrderLineData.getAttributeSetInstance());

    distributionOrderLine.setDistributionOrder(distributionOrder);
    OBDal.getInstance().save(distributionOrderLine);
    distributionOrder.getOBDODistributionOrderLineList().add(distributionOrderLine);
    OBDal.getInstance().save(distributionOrder);
    OBDal.getInstance().flush();
    return distributionOrderLine;
  }

  /**
   * Creates and saves an Issue or Received Distribution Order Header based on the data given as a
   * parameter
   */
  public static InternalMovement createAndSaveIssueOrReceiveDOHeader(
      IssueOrReceiptDOHeaderData issueOrReceiptDOHeaderData) {
    InternalMovement issueOrReceiptDO = OBProvider.getInstance().get(InternalMovement.class);
    issueOrReceiptDO.setClient(issueOrReceiptDOHeaderData.getOrganization().getClient());
    issueOrReceiptDO.setOrganization(issueOrReceiptDOHeaderData.getOrganization());
    issueOrReceiptDO.setName(issueOrReceiptDOHeaderData.getName());
    issueOrReceiptDO.setDocumentNo(issueOrReceiptDOHeaderData.getName());
    issueOrReceiptDO.setDescription(issueOrReceiptDOHeaderData.getDescription());
    issueOrReceiptDO.setMovementDate(issueOrReceiptDOHeaderData.getMovementDate());
    issueOrReceiptDO.setObdoDistorder(issueOrReceiptDOHeaderData.getRelatedDistributionOrder());
    issueOrReceiptDO.setObdoIntransitLocator(issueOrReceiptDOHeaderData.getInTransitBin());
    issueOrReceiptDO.setObdoIssotrx(issueOrReceiptDOHeaderData.isSalesTransaction());
    issueOrReceiptDO.setObdoIssueMovement(issueOrReceiptDOHeaderData.getRelatedIssueDO());
    issueOrReceiptDO.setObdoIsdomovement(true);
    OBDal.getInstance().save(issueOrReceiptDO);
    return issueOrReceiptDO;
  }

  /**
   * Creates and saves an Issue or Receipt Distribution Order Line based on the data given as a
   * parameter. It links the line to the given Issue or Receipt Distribution Order Parameter
   */
  public static InternalMovementLine createAndInsertIssueOrReceiptDOLine(
      IssueOrReceiptDOLineData issueOrReceiptDOLineData, InternalMovement issueOrReceiptDO) {
    InternalMovementLine issueOrReceiptDOLine = OBProvider.getInstance()
        .get(InternalMovementLine.class);
    issueOrReceiptDOLine.setClient(issueOrReceiptDOLineData.getOrganization().getClient());
    issueOrReceiptDOLine.setOrganization(issueOrReceiptDOLineData.getOrganization());
    issueOrReceiptDOLine.setLineNo(issueOrReceiptDOLineData.getLineNo());
    issueOrReceiptDOLine.setProduct(issueOrReceiptDOLineData.getProduct());
    issueOrReceiptDOLine.setAttributeSetValue(issueOrReceiptDOLineData.getAttributeSetInstance());
    issueOrReceiptDOLine.setUOM(issueOrReceiptDOLineData.getUom());
    issueOrReceiptDOLine.setMovementQuantity(issueOrReceiptDOLineData.getMovementQty());
    issueOrReceiptDOLine.setStorageBin(issueOrReceiptDOLineData.getStorageBinFrom());
    issueOrReceiptDOLine.setNewStorageBin(issueOrReceiptDOLineData.getStorageBinTo());
    issueOrReceiptDOLine
        .setObdoDistorderline(issueOrReceiptDOLineData.getRelatedDistributionOrderLine());

    issueOrReceiptDOLine.setMovement(issueOrReceiptDO);
    OBDal.getInstance().save(issueOrReceiptDOLine);
    issueOrReceiptDO.getMaterialMgmtInternalMovementLineList().add(issueOrReceiptDOLine);
    OBDal.getInstance().save(issueOrReceiptDO);
    OBDal.getInstance().flush();
    return issueOrReceiptDOLine;
  }

  /**
   * Creates and saves a Goods Receipt Header based on the data given as a parameter
   */
  public static ShipmentInOut createAndSaveGoodsReceiptHeader(
      ShipmentInOutHeaderData shipmentInOutHeaderData) {
    ShipmentInOut goodsReceipt = OBProvider.getInstance().get(ShipmentInOut.class);
    goodsReceipt.setClient(shipmentInOutHeaderData.getOrganization().getClient());
    goodsReceipt.setOrganization(shipmentInOutHeaderData.getOrganization());
    goodsReceipt.setDocumentNo(shipmentInOutHeaderData.getDocNo());
    goodsReceipt.setDocumentType(shipmentInOutHeaderData.getDocType());
    goodsReceipt.setWarehouse(shipmentInOutHeaderData.getWarehouse());
    goodsReceipt.setBusinessPartner(shipmentInOutHeaderData.getBusinessPartner());
    goodsReceipt.setPartnerAddress(
        shipmentInOutHeaderData.getBusinessPartner().getBusinessPartnerLocationList().get(0));
    goodsReceipt.setMovementDate(new Date());
    goodsReceipt.setAccountingDate(new Date());
    goodsReceipt.setSalesTransaction(false);
    OBDal.getInstance().save(goodsReceipt);
    return goodsReceipt;
  }

  /**
   * Creates and saves a Goods Receipt Line based on the data given as a parameter. It links the
   * line to the given Goods Receipt parameter
   */
  public static ShipmentInOut createAndInsertGoodsReceiptLine(
      ShipmentInOutLineData shipmentInoutLineData, ShipmentInOut goodsReceipt) {
    ShipmentInOutLine goodsReceiptLine = OBProvider.getInstance().get(ShipmentInOutLine.class);
    goodsReceiptLine.setClient(shipmentInoutLineData.getOrganization().getClient());
    goodsReceiptLine.setOrganization(shipmentInoutLineData.getOrganization());
    goodsReceiptLine.setLineNo(shipmentInoutLineData.getLineNo());
    goodsReceiptLine.setProduct(shipmentInoutLineData.getProduct());
    goodsReceiptLine.setMovementQuantity(shipmentInoutLineData.getMovementQty());
    goodsReceiptLine.setUOM(shipmentInoutLineData.getUom());
    goodsReceiptLine.setStorageBin(shipmentInoutLineData.getStorageBin());

    goodsReceiptLine.setShipmentReceipt(goodsReceipt);
    OBDal.getInstance().save(goodsReceiptLine);
    goodsReceipt.getMaterialMgmtShipmentInOutLineList().add(goodsReceiptLine);
    OBDal.getInstance().save(goodsReceipt);
    OBDal.getInstance().flush();
    return goodsReceipt;
  }

  /**
   * Processes a Goods Receipt/Shipment and throws an exception if there are any issues while
   * processing it
   */
  public static void processShipmentInOut(ShipmentInOut goodsReceipt) {
    final Process process = OBDal.getInstance().get(Process.class, PROCESS__M_MINOUT_POST);
    final ProcessInstance pinstance = CallProcess.getInstance()
        .call(process, goodsReceipt.getId(), null);
    final OBError result = OBMessageUtils.getProcessInstanceMessage(pinstance);
    if (StringUtils.equals("Error", result.getType())) {
      throw new OBException(result.getMessage());
    }
  }

  /**
   * Processes the given Distribution Order and verifies that it has been correctly processed
   */
  public static void processDistributionOrderAndVerifyIsProcessed(
      DistributionOrder distributionOrder) {
    ProcessDistributionOrderUtil.processDistributionOrder(distributionOrder);
    OBDal.getInstance().flush();
    assertDistributionOrderProcessed(distributionOrder);
  }

  /**
   * Processes the given Issue Distribution Order and verifies that it has been correctly processed
   */
  public static void processIssueDOAndVerifyIsProcessed(InternalMovement issueDO)
      throws JSONException {
    ProcessIssue.processIssueDO(issueDO);
    OBDal.getInstance().flush();
    // Need to refresh the object in order to retrieve the latest changes done to it
    OBDal.getInstance().refresh(issueDO);
    assertIssueOrReceiptDOIsProcessed(issueDO);
  }

  /**
   * Processes the given Receive Distribution Order and verifies that it has been correctly
   * processed
   */
  public static void processReceiveDOAndVerifyIsProcessed(InternalMovement receiveDO,
      String receptionBinID) throws JSONException {
    ProcessReceipt.processReceiveDO(receiveDO, receptionBinID);
    OBDal.getInstance().flush();
    // Need to refresh the object in order to retrieve the latest changes done to it
    OBDal.getInstance().refresh(receiveDO);
    assertIssueOrReceiptDOIsProcessed(receiveDO);
  }

  /**
   * Update Quantity confirmed of the given Distribution Order Issue, confirm the document and check
   * that the update has been done correctly
   */
  public static void updateConfirmedQtyAndVerifyRelatedDOIsUpdated(
      DistributionOrderLine issueDistributionOrderLine, BigDecimal confirmedQty) {
    DistributionOrderLine receiptDistributionOrderLine = issueDistributionOrderLine
        .getReferencedDistorderline();
    issueDistributionOrderLine.setQtyConfirmed(confirmedQty);
    confirmDistributionOrderAndVerifyStatusUpdated(
        issueDistributionOrderLine.getDistributionOrder());
    // Need to refresh the object in order to retrieve the latest changes done to it
    OBDal.getInstance().refresh(receiptDistributionOrderLine);
    assertThatConfirmedQtyHasBeenUpdatedCorrectly(receiptDistributionOrderLine, confirmedQty);

  }

  /**
   * Update Quantity confirmed of the given Distribution Order Issue, confirm the document and check
   * that the update has been done correctly
   */
  public static void updateConfirmedQty(DistributionOrderLine issueDistributionOrderLine,
      BigDecimal confirmedQty) {
    DistributionOrderLine receiptDistributionOrderLine = issueDistributionOrderLine
        .getReferencedDistorderline();
    issueDistributionOrderLine.setQtyConfirmed(confirmedQty);
    // Need to refresh the object in order to retrieve the latest changes done to it
    OBDal.getInstance().refresh(receiptDistributionOrderLine);
  }

  /**
   * Confirms the given Distribution Order and verifies that the status has changed to it and the
   * related Distribution Order
   * 
   * @param distributionOrder
   */
  public static void confirmDistributionOrderAndVerifyStatusUpdated(
      DistributionOrder distributionOrder) {
    ProcessDistributionOrderUtil.processDistributionOrder(distributionOrder);
    OBDal.getInstance().flush();
    assertDistributionOrderConfirmed(distributionOrder);
    assertDistributionOrderConfirmed(distributionOrder.getReferencedDistorder());
  }

  /**
   * Close a given Distribution Order and check that the status has been updated properly
   */
  public static void closeDistributionOrderAndCheckStatusUpdated(
      DistributionOrder distributionOrder) {
    ProcessDistributionOrderUtil.processDistributionOrder(distributionOrder);
    OBDal.getInstance().flush();
    assertDistributionOrderHasClosedStatus(distributionOrder);
  }

  /**
   * Returns the Document Type for Receipt Distribution Orders
   */
  public static DocumentType getDistributionOrderReceiptDocType() {
    OBCriteria<DocumentType> obc = OBDal.getInstance().createCriteria(DocumentType.class);
    obc.add(Restrictions.eq(DocumentType.PROPERTY_NAME, DO_RECEIPT_DOCTYPE_NAME));
    return (DocumentType) obc.uniqueResult();
  }

  /**
   * Returns the Document Type for Receipt Distribution Issues
   */
  public static DocumentType getDistributionOrderIssueDocType() {
    OBCriteria<DocumentType> obc = OBDal.getInstance().createCriteria(DocumentType.class);
    obc.add(Restrictions.eq(DocumentType.PROPERTY_NAME, DO_ISSUE_DOCTYPE_NAME));
    return (DocumentType) obc.uniqueResult();
  }

  /**
   * Returns the ID of the record from DistributionOrderView that is related to the given Issue
   * Distribution Order
   */
  public static String getIDOfViewRecordForIssueDO(InternalMovement issueDistributionOrder) {
    DistributionOrder distributionOrderIssue = issueDistributionOrder.getObdoDistorder();
    DistributionOrder distributionOrderReceipt = distributionOrderIssue.getReferencedDistorder();
    OBCriteria<DistributionOrderV> obc = OBDal.getInstance()
        .createCriteria(DistributionOrderV.class);
    obc.add(Restrictions.eq(DistributionOrderV.PROPERTY_DOCUMENTNO,
        issueDistributionOrder.getDocumentNo()));
    obc.add(
        Restrictions.eq(DistributionOrderV.PROPERTY_ISSUE, distributionOrderIssue.getDocumentNo()));
    obc.add(Restrictions.eq(DistributionOrderV.PROPERTY_RECEIPT,
        distributionOrderReceipt.getDocumentNo()));
    DistributionOrderV distributionOrderView = (DistributionOrderV) obc.uniqueResult();
    return distributionOrderView.getId();
  }

  /**
   * Returns the next Document Number for the Distribution Order with the given Document No.
   */
  public static String getNextDocNoForDistributionOrder(String testDocNo) {
    OBCriteria<DistributionOrder> obc = OBDal.getInstance().createCriteria(DistributionOrder.class);
    obc.add(Restrictions.like(DistributionOrder.PROPERTY_DOCUMENTNO, testDocNo + "-%"));
    return testDocNo + "-" + obc.list().size();
  }

  /**
   * Returns the next Document Number for the Issue or Receive DO with the given Document No.
   */
  public static String getNextDocNoForIssueOrReceiptDO(String testDocNo) {
    OBCriteria<InternalMovement> obc = OBDal.getInstance().createCriteria(InternalMovement.class);
    obc.add(Restrictions.like(InternalMovement.PROPERTY_NAME, testDocNo + "-%"));
    return testDocNo + "-" + obc.list().size();
  }

  /**
   * Returns the next Document Number for the Goods Receipt/Shipment with the given Document No.
   */
  public static String getNextDocNoForShipmentInOut(String testDocNo) {
    OBCriteria<ShipmentInOut> obc = OBDal.getInstance().createCriteria(ShipmentInOut.class);
    obc.add(Restrictions.like(ShipmentInOut.PROPERTY_DOCUMENTNO, testDocNo + "-%"));
    return testDocNo + "-" + obc.list().size();
  }

  /**
   * Returns the next Line No. for the given Distribution Order
   */
  public static long getNextLineNoForDistributionOrder(DistributionOrder distributionOrder) {
    OBCriteria<DistributionOrderLine> obc = OBDal.getInstance()
        .createCriteria(DistributionOrderLine.class);
    obc.add(Restrictions.eq(DistributionOrderLine.PROPERTY_DISTRIBUTIONORDER, distributionOrder));
    obc.addOrderBy(DistributionOrderLine.PROPERTY_LINENO, false);
    obc.setMaxResults(1);
    DistributionOrderLine lastDOLine = (DistributionOrderLine) obc.uniqueResult();
    return lastDOLine == null ? 10L : lastDOLine.getLineNo();
  }

  /**
   * Returns the next Line No. for the given Issue or Receive DO
   */
  public static long getNextLineNoForIssueOrReceiptDO(InternalMovement issueOrReceiptDO) {
    OBCriteria<InternalMovementLine> obc = OBDal.getInstance()
        .createCriteria(InternalMovementLine.class);
    obc.add(Restrictions.eq(InternalMovementLine.PROPERTY_MOVEMENT, issueOrReceiptDO));
    obc.addOrderBy(InternalMovementLine.PROPERTY_LINENO, false);
    obc.setMaxResults(1);
    InternalMovementLine lastDOLine = (InternalMovementLine) obc.uniqueResult();
    return lastDOLine == null ? 10L : lastDOLine.getLineNo();
  }

  /**
   * Returns the next Line No. for the given Goods Receipt
   */
  public static long getNextLineNoForGoodsReceipt(ShipmentInOut goodsReceipt) {
    OBCriteria<ShipmentInOutLine> obc = OBDal.getInstance().createCriteria(ShipmentInOutLine.class);
    obc.add(Restrictions.eq(ShipmentInOutLine.PROPERTY_SHIPMENTRECEIPT, goodsReceipt));
    obc.addOrderBy(ShipmentInOutLine.PROPERTY_LINENO, false);
    obc.setMaxResults(1);
    ShipmentInOutLine lastDOLine = (ShipmentInOutLine) obc.uniqueResult();
    return lastDOLine == null ? 10L : lastDOLine.getLineNo();
  }

  /**
   * Returns a new Goods Receipt/Shipment based on the given one. It is a clone of the first one but
   * in a not completed status
   * 
   * @param mInoutId
   *          Id of original Goods Receipt/Shipment to clone
   * @param docNo
   *          docNo to set to the new Goods Receipt/Shipment
   * @return a Goods Receipt/Shipment not completed
   */
  public static ShipmentInOut cloneReceiptShipment(String mInoutId, String docNo) {
    ShipmentInOut oldInOut = OBDal.getInstance().get(ShipmentInOut.class, mInoutId);
    ShipmentInOut newInOut = (ShipmentInOut) DalUtil.copy(oldInOut, false);
    int numberOfShipmentsWithSameDocNo = DistributionOrderTestUtils.getNumberOfShipments(docNo) + 1;

    newInOut.setId(SequenceIdData.getUUID());
    newInOut.setDocumentNo(docNo + "-" + numberOfShipmentsWithSameDocNo);
    newInOut.setDocumentStatus(DRAFT_DOCSTATUS);
    newInOut.setDocumentAction(COMPLETED_DOCSTATUS);
    newInOut.setProcessed(false);
    newInOut.setMovementDate(new Date());
    newInOut.setOrderDate(new Date());
    newInOut.setNewOBObject(true);
    newInOut.setSalesOrder(null);

    OBDal.getInstance().save(newInOut);

    for (ShipmentInOutLine line : oldInOut.getMaterialMgmtShipmentInOutLineList()) {
      cloneReceiptShipmentLine(line, newInOut);
    }

    OBDal.getInstance().flush();
    return newInOut;
  }

  /**
   * Returns the number of Goods Receipts/Shipments with same Document Number
   */
  public static int getNumberOfShipments(String docNo) {
    try {
      final OBCriteria<ShipmentInOut> criteria = OBDal.getInstance()
          .createCriteria(ShipmentInOut.class);
      criteria.add(Restrictions.like(ShipmentInOut.PROPERTY_DOCUMENTNO, docNo + "-%"));
      return criteria.list().size();
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  /**
   * Returns a new Goods Receipt/Shipment Line based on the given one. It is a clone of the first
   * one but with different product
   * 
   * @param oldLine
   *          Original Goods Receipt/Shipment
   * @param newInOut
   *          new Goods Receipt/Shipment (a clone of the original one)
   * @return A new Goods Receipt/Shipment Line clone based on the original one
   */
  public static ShipmentInOutLine cloneReceiptShipmentLine(ShipmentInOutLine oldLine,
      ShipmentInOut newInOut) {
    ShipmentInOutLine newLine = (ShipmentInOutLine) DalUtil.copy(oldLine, false);

    newLine.setId(SequenceIdData.getUUID());
    newLine.setShipmentReceipt(newInOut);
    newLine.setNewOBObject(true);
    newLine.setSalesOrderLine(null);

    OBDal.getInstance().save(newLine);
    newInOut.getMaterialMgmtShipmentInOutLineList().add(newLine);

    return newLine;
  }

  /**
   * Asserts That Distribution Order has confirmed Status and that the related Distribution Order
   * Document has confirmed status too
   */
  public static void assertDistributionOrderConfirmed(DistributionOrder issueDistributionOrder) {
    assertDistributionOrderHasConfirmedStatus(issueDistributionOrder);
    DistributionOrder receiptDistributionOrder = issueDistributionOrder.getReferencedDistorder();
    // Need to refresh the object in order to retrieve the latest changes done to it
    OBDal.getInstance().refresh(receiptDistributionOrder);
    assertDistributionOrderHasConfirmedStatus(receiptDistributionOrder);
  }

  /**
   * Asserts that Distribution Order has process status and that the related Distribution Order
   * Document has the corresponding Status
   */
  public static void assertDistributionOrderProcessed(DistributionOrder distributionOrder) {
    if (distributionOrder.isSalesTransaction()) {
      assertDistributionOrderHasConfirmedStatus(distributionOrder);
      assertThatDistributionOrderHasIssuedStatus(distributionOrder, 0L);
    } else {
      assertDistributionOrderHasCompletedStatus(distributionOrder);
      assertThatDistributionOrderHasReceiptStatus(distributionOrder, 0L);
    }
  }

  /**
   * Asserts that the related Distribution Order Documents has been updated properly after a Receive
   * DO has been done
   */
  public static void assertThatReceiveDoLineHasUpdatedRelatedDocs(
      InternalMovementLine receiveDOLine, BigDecimal totalReceivedQty, long receivedPercentage) {
    DistributionOrderLine distributionOrderIssueLine = receiveDOLine.getObdoDistorderline()
        .getReferencedDistorderline();
    DistributionOrder distributionOrderIssue = distributionOrderIssueLine.getDistributionOrder();
    DistributionOrderLine distributionOrderReceiptLine = distributionOrderIssueLine
        .getReferencedDistorderline();
    DistributionOrder distributionOrderReceipt = distributionOrderReceiptLine
        .getDistributionOrder();
    // Need to refresh the objects in order to retrieve the latest changes done to it
    OBDal.getInstance().refresh(distributionOrderIssueLine);
    OBDal.getInstance().refresh(distributionOrderIssue);
    OBDal.getInstance().refresh(distributionOrderReceiptLine);
    OBDal.getInstance().refresh(distributionOrderReceipt);

    assertThatDistributionOrderLineHasReceiptQty(distributionOrderIssueLine, totalReceivedQty);
    assertThatDistributionOrderLineHasReceiptQty(distributionOrderReceiptLine, totalReceivedQty);
    assertThatDistributionOrderHasReceiptStatus(distributionOrderReceipt, receivedPercentage);
  }

  /**
   * Asserts that the related Distribution Order Documents has been updated properly after a Issue
   * DO has been done
   */
  public static void assertThatIssueDoLineHasUpdatedRelatedDocs(InternalMovementLine issueDOLine,
      BigDecimal totalIssuedQty, long issuedPercentage) {
    DistributionOrderLine distributionOrderIssueLine = issueDOLine.getObdoDistorderline();
    DistributionOrder distributionOrderIssue = distributionOrderIssueLine.getDistributionOrder();
    DistributionOrderLine distributionOrderReceiptLine = distributionOrderIssueLine
        .getReferencedDistorderline();
    DistributionOrder distributionOrderReceipt = distributionOrderReceiptLine
        .getDistributionOrder();
    // Need to refresh the objects in order to retrieve the latest changes done to it
    OBDal.getInstance().refresh(distributionOrderIssueLine);
    OBDal.getInstance().refresh(distributionOrderIssue);
    OBDal.getInstance().refresh(distributionOrderReceiptLine);
    OBDal.getInstance().refresh(distributionOrderReceipt);

    assertThatDistributionOrderLineHasIssuedQty(distributionOrderIssueLine, totalIssuedQty);
    assertThatDistributionOrderHasIssuedStatus(distributionOrderIssue, issuedPercentage);
    assertThatDistributionOrderLineHasIssuedQty(distributionOrderReceiptLine, totalIssuedQty);
  }

  public static void assertDistributionOrderHasRequestedStatus(
      DistributionOrder distributionOrder) {
    assertThat("Distribution Order Status is Requested': ", distributionOrder.getDocumentStatus(),
        equalTo(REQUESTED_DOCSTATUS));
  }

  public static void assertDistributionOrderHasCompletedStatus(
      DistributionOrder distributionOrder) {
    assertThat("Distribution Order Status is Completed': ", distributionOrder.getDocumentStatus(),
        equalTo(COMPLETED_DOCSTATUS));
  }

  public static void assertDistributionOrderHasConfirmedStatus(
      DistributionOrder distributionOrder) {
    assertThat("Distribution Order Status is confirmed': ", distributionOrder.getDocumentStatus(),
        equalTo(CONFIRMED_DOCSTATUS));
  }

  public static void assertDistributionOrderHasClosedStatus(DistributionOrder distributionOrder) {
    assertThat("Distribution Order Status is closed': ", distributionOrder.getDocumentStatus(),
        equalTo(CLOSED_DOCSTATUS));
  }

  public static void assertIssueOrReceiptDOIsProcessed(InternalMovement issueOrReceiveDO) {
    assertThat("Issue or Receipt DO has processed = 'Y'", issueOrReceiveDO.isProcessed(),
        equalTo(true));

  }

  public static void assertThatDistributionOrderHasIssuedStatus(DistributionOrder distributionOrder,
      long issuedPercentage) {
    assertThat("Issue Status of Document is: " + issuedPercentage,
        distributionOrder.getIssueStatus(), equalTo(issuedPercentage));
  }

  public static void assertThatDistributionOrderHasReceiptStatus(
      DistributionOrder distributionOrder, long receivedPercentage) {
    assertThat("Receipt Status of Document is: " + receivedPercentage,
        distributionOrder.getReceiptStatus(), equalTo(receivedPercentage));
  }

  public static void assertThatDistributionOrderLineHasIssuedQty(
      DistributionOrderLine distributionOrderLine, BigDecimal totalIssuedQty) {
    assertThat("Issued Quantity of Line is: " + totalIssuedQty,
        distributionOrderLine.getQtyIssued(), equalTo(totalIssuedQty));
  }

  public static void assertThatDistributionOrderLineHasReceiptQty(
      DistributionOrderLine distributionOrderLine, BigDecimal totalReceivedQty) {
    assertThat("Receipt Quantity of Line is: " + totalReceivedQty,
        distributionOrderLine.getQtyReceived(), equalTo(totalReceivedQty));
  }

  public static void assertDistributionOrderHasNLines(DistributionOrder distributionOrder,
      int numberOfDOLines) {
    int realNumberOfDOLines = distributionOrder.getOBDODistributionOrderLineList().size();
    assertThat("Distribution Order has " + numberOfDOLines + " lines", realNumberOfDOLines,
        equalTo(numberOfDOLines));

  }

  public static void assertDistributionOrderIsNotNull(DistributionOrder distributionOrder) {
    assertThat("Distribution Order is not null: ", distributionOrder, is(notNullValue()));
  }

  public static void assertDistributionOrderCreatedIsRelatedToOriginalDO(
      DistributionOrder createdDistributionOrder, DistributionOrder origDistributionOrder) {
    assertThat("Created Distribution Order is related to original Distribution Order",
        origDistributionOrder, equalTo(createdDistributionOrder.getReferencedDistorder()));
  }

  public static void assertThatConfirmedQtyHasBeenUpdatedCorrectly(
      DistributionOrderLine receiptDistributionOrderLine, BigDecimal confirmedQty) {
    assertThat("Confirmed Quantity must be updated correctly to: " + confirmedQty,
        receiptDistributionOrderLine.getQtyConfirmed(), equalTo(confirmedQty));
  }

  public static void unsetDeleteNonConfirmedTaskAndReservationsPreference() {
    setValueOfDeleteNonConfirmedTaskAndReservationsPreference("N");
  }

  public static void setDeleteNonConfirmedTaskAndReservationsPreference() {
    setValueOfDeleteNonConfirmedTaskAndReservationsPreference("Y");
  }

  private static void setValueOfDeleteNonConfirmedTaskAndReservationsPreference(
      final String preferenceValue) {
    final Preference deleteNonConfirmedTaskAndReservations = Preferences.setPreferenceValue(
        "OBDOA_DeleteNonConfTaskAndResWhenCloseDO", preferenceValue, true,
        OBContext.getOBContext().getCurrentClient(), null, null, null, null,
        RequestContext.get().getVariablesSecureApp());
    deleteNonConfirmedTaskAndReservations.setClient(OBContext.getOBContext().getCurrentClient());
    OBDal.getInstance().flush();
  }
}
